﻿using log4net;
using Myriagonal.Net.Core.Logging;

namespace Myriagonal.Net.Logging.Log4Net {
    public class Log4NetLogger : Logger {
        private readonly ILog log;

        public Log4NetLogger(ILog log) {
            this.log = log;
        }

        public void Trace(string message) {
            log.Debug(message);
        }

        public void Debug(string message) {
            log.Debug(message);
        }

        public void Info(string message) {
            log.Info(message);
        }

        public void Warn(string message) {
            log.Warn(message);
        }

        public void Error(string message) {
            log.Error(message);
        }
    }
}