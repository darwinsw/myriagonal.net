﻿using log4net;
using Myriagonal.Net.Core.Logging;

namespace Myriagonal.Net.Logging.Log4Net {
    public class Log4NetLoggerFactory : LoggerFactory {
        public Logger GetLogger<T>() {
            return new Log4NetLogger(LogManager.GetLogger(typeof(T)));
        }
    }
}