﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Myriagonal.Net.RabbitMq;
using Newtonsoft.Json;
using NUnit.Framework;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Myriagonal.Net.IntegrationTests.RabbitMq {
    [TestFixture]
    public class RabbitMqPublisherAndConsumerTest {
        const string TheExchangeName = "test-exchange";
        const string TheQueueName = "test-queue";

        private static readonly ConnectionFactory ConnectionFactory = new ConnectionFactory() {
            HostName = "rabbitmqtest",
            UserName = "guest",
            Password = "guest",
            VirtualHost = "integration-tests"
        };

        private static readonly RabbitMqPublisherConfiguration RabbitMqPublisherConfiguration = new RabbitMqPublisherConfiguration {
            ConnectionUrl = $"amqp://{ConnectionFactory.UserName}:{ConnectionFactory.Password}@{ConnectionFactory.HostName}/{ConnectionFactory.VirtualHost}",
            ExchangeName = TheExchangeName,
            QueueName = TheQueueName
        };
        
        private static readonly RabbitMqConsumerConfiguration RabbitMqConsumerConfiguration = new RabbitMqConsumerConfiguration {
            ConnectionUrl = $"amqp://{ConnectionFactory.UserName}:{ConnectionFactory.Password}@{ConnectionFactory.HostName}/{ConnectionFactory.VirtualHost}",
            QueueName = TheQueueName
        };

        private RabbitMqPublisher publisher;
        private RabbitMqConsumer<Message> consumer;
        private Message receivedMessage;

        [SetUp]
        public async Task InitPubslisherAndConsumer() {
            publisher = new RabbitMqPublisher(RabbitMqPublisherConfiguration);
            await publisher.Start();
            consumer = new RabbitMqConsumer<Message>(RabbitMqConsumerConfiguration, (msg) => Task.Run( () => receivedMessage = msg));
            await consumer.Start();
        }

        [TearDown]
        public async Task DisposePublisherAndConnection() {
            await publisher.Stop();
            await consumer.Stop();
        }

        [Test]
        public async Task UsePublisher() {
            var message = new Message("Pietro Martinelli", 19);
            await publisher.PublishAsync(message);
            await Task.Delay(TimeSpan.FromMilliseconds(200));
            Assert.That(receivedMessage, Is.Not.Null);
            Assert.That(receivedMessage.TheText, Is.EqualTo(message.TheText));
            Assert.That(receivedMessage.TheNumber, Is.EqualTo(message.TheNumber));
        }
    }

    public class Message {
        public Message(string aText, int anInt) {
            TheText = aText;
            TheNumber = anInt;
        }

        public string TheText { get; set; }
        public int TheNumber { get; set; }
    }
}