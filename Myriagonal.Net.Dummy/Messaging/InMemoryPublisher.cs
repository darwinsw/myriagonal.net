﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Myriagonal.Net.Core.Messaging;

namespace Myriagonal.Net.Dummy.Messaging {
    public class InMemoryPublisher : Publisher {
        public IList<object> Messages { get; }

        public InMemoryPublisher() {
            Messages = new List<object>();
        }

        public Task PublishAsync<T>(T message) {
            return Task.Run(() => Messages.Add(message));
        }

        public Task Start() {
            return Task.FromResult(0);
        }

        public Task Stop() {
            return Task.FromResult(0);
        }
    }
}