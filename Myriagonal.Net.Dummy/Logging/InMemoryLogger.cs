﻿using System.Collections.Generic;
using Myriagonal.Net.Core.Logging;

namespace Myriagonal.Net.Dummy.Logging {
    public class InMemoryLogger : Logger {
        public InMemoryLogger() {
            Entries = new List<LogEntry>();
        }

        public IList<LogEntry> Entries { get; }
        
        public void Log(LogLevel level, string message) {
            Entries.Add(new LogEntry(level, message));
        }

        public void Trace(string message) {
            Log(LogLevel.Trace, message);
        }

        public void Debug(string message) {
            Log(LogLevel.Debug, message);
        }

        public void Info(string message) {
            Log(LogLevel.Info, message);
        }

        public void Warn(string message) {
            Log(LogLevel.Warn, message);
        }

        public void Error(string message) {
            Log(LogLevel.Error, message);
        }

        public class LogEntry {
            public LogEntry(LogLevel level, string message) {
                Level = level;
                Message = message;
            }

            public LogLevel Level { get; }
            public string Message { get; }
        }
    }
}