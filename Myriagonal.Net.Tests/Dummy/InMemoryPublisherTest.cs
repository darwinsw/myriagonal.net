﻿using System.Threading.Tasks;
using Myriagonal.Net.Dummy.Messaging;
using Myriagonal.Net.Core.Messaging;
using NUnit.Framework;

namespace Myriagonal.Net.Tests.Dummy {
    [TestFixture]
    public class InMemoryPublisherTest {
        [Test]
        public async Task PubslishedMessagesAreAvailableInPublicList() {
            Publisher publisher = new InMemoryPublisher();
            const string msg = "foo bar buzz";
            await publisher.PublishAsync(msg);
            InMemoryPublisher spy = (InMemoryPublisher) publisher;
            Assert.That(spy.Messages.Count, Is.EqualTo(1));
            Assert.That(spy.Messages[0], Is.EqualTo(msg));
        }
        
        [Test]
        public async Task CanPubslishMessagesOfDifferentTypes() {
            Publisher publisher = new InMemoryPublisher();
            const string textMessage = "foo bar buzz";
            const int numericMessage = 19;
            object objectMessage = new object();
            await publisher.PublishAsync(textMessage);
            await publisher.PublishAsync(numericMessage);
            await publisher.PublishAsync(objectMessage);
            InMemoryPublisher spy = (InMemoryPublisher) publisher;
            Assert.That(spy.Messages.Count, Is.EqualTo(3));
            Assert.That(spy.Messages[0], Is.EqualTo(textMessage));
            Assert.That(spy.Messages[1], Is.EqualTo(numericMessage));
            Assert.That(spy.Messages[2], Is.EqualTo(objectMessage));
        }
    }
}