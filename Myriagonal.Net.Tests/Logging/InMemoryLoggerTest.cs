﻿using Myriagonal.Net.Core.Logging;
using Myriagonal.Net.Dummy.Logging;
using NUnit.Framework;

namespace Myriagonal.Net.Tests.Logging {
    [TestFixture]
    public class InMemoryLoggerTest {
        [Test]
        public void ShouldAppendAllLogEntriesToInMemoryList() {
            InMemoryLogger logger = new InMemoryLogger();
            logger.Debug("Sample debug");
            logger.Warn("Sample warn");
            logger.Error("Sample error");
            Assert.That(logger.Entries.Count, Is.EqualTo(3));
        }
        
        [Test]
        public void ShouldMantainLogEntryMessagesInMemoryList() {
            InMemoryLogger logger = new InMemoryLogger();
            logger.Debug("Sample debug");
            logger.Warn("Sample warn");
            logger.Error("Sample error");
            Assert.That(logger.Entries[2].Message, Is.EqualTo("Sample error"));
        }
        
        [Test]
        public void ShouldMantainLogEntryLevelssInMemoryList() {
            InMemoryLogger logger = new InMemoryLogger();
            logger.Debug("Sample debug");
            logger.Warn("Sample warn");
            logger.Error("Sample error");
            Assert.That(logger.Entries[1].Level, Is.EqualTo(LogLevel.Warn));
        }
    }
}