﻿using System;
using System.IO;
using Myriagonal.Net.Core.Logging;
using Myriagonal.Net.Sys;
using NUnit.Framework;

namespace Myriagonal.Net.Tests.Sys {
    [TestFixture]
    public class SystemConsoleLoggerTest {
        private const string LogMessage = "Test log message";
        
        private TextWriter original;
        private TextWriter mocked;
        private Logger logger;
        
        [SetUp]
        public void InitializeMockedWriter() {
            original = Console.Out;
            mocked = new StringWriter();
            Console.SetOut(mocked);
        }

        [SetUp]
        public void InitializeLogger() {
            logger = new SystemConsoleLogger();
        }

        [TearDown]
        public void ResetOriginalWriter() {
            Console.SetOut(original);
        }

        [Test]
        public void CanTrace() {
            logger.Trace(LogMessage);
            Assert.That(mocked.ToString(), Is.EqualTo($"TRACE {LogMessage}{Environment.NewLine}"));
        }
        [Test]
        public void CanLogDebug() {
            logger.Debug(LogMessage);
            Assert.That(mocked.ToString(), Is.EqualTo($"DEBUG {LogMessage}{Environment.NewLine}"));
        }
        [Test]
        public void CanLogInfo() {
            logger.Info(LogMessage);
            Assert.That(mocked.ToString(), Is.EqualTo($"INFO {LogMessage}{Environment.NewLine}"));
        }
        [Test]
        public void CanLogWarn() {
            logger.Warn(LogMessage);
            Assert.That(mocked.ToString(), Is.EqualTo($"WARN {LogMessage}{Environment.NewLine}"));
        }
        [Test]
        public void CanLogError() {
            logger.Error(LogMessage);
            Assert.That(mocked.ToString(), Is.EqualTo($"ERROR {LogMessage}{Environment.NewLine}"));
        }
    }
}