# README #

Myriagonal.Net: a simple collection of classes supporting the *hexagonal* architecture style.

| Pipeline | Status |
| ------------- |:--------------|
| Master pipeline      | [![pipeline status](https://gitlab.com/darwinsw/myriagonal.net/badges/master/pipeline.svg)](https://gitlab.com/darwinsw/myriagonal.net/commits/master) |
| Release pipeline | [![pipeline status](https://gitlab.com/darwinsw/myriagonal.net/badges/release/pipeline.svg)](https://gitlab.com/darwinsw/myriagonal.net/commits/release) |
