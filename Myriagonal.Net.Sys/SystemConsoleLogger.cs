﻿using System;
using Myriagonal.Net.Core.Logging;

namespace Myriagonal.Net.Sys {
    public class SystemConsoleLogger : Logger {
        public void Log(LogLevel level, string message) {
            Console.WriteLine($"{level.ToString().ToUpperInvariant()} {message}");
        }

        public void Trace(string message) {
            Log(LogLevel.Trace, message);
        }

        public void Debug(string message) {
            Log(LogLevel.Debug, message);
        }

        public void Info(string message) {
            Log(LogLevel.Info, message);
        }

        public void Warn(string message) {
            Log(LogLevel.Warn, message);
        }

        public void Error(string message) {
            Log(LogLevel.Error, message);
        }
    }
}