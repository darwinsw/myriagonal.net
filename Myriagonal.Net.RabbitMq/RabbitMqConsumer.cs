﻿using System;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Myriagonal.Net.RabbitMq {
    public class RabbitMqConsumer<TMessage> {
        private readonly RabbitMqConsumerConfiguration config;
        private readonly Func<TMessage, Task> handler;
        private readonly ConnectionFactory connectionFactory;
        private IConnection connection;
        private IModel channel;

        public RabbitMqConsumer(RabbitMqConsumerConfiguration config, Func<TMessage, Task> handler) {
            this.config = config;
            this.handler = handler;
            connectionFactory = new ConnectionFactory() {
                Uri = new Uri(config.ConnectionUrl)
            };
        }

        public Task Start() {
            return Task.Run(() => {
                connection = connectionFactory.CreateConnection(); 
                channel = connection.CreateModel();
                
                EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (ch, ea) =>
                {
                    byte[] body = ea.Body;
                    string receivedText = Encoding.UTF8.GetString(body);
                    TMessage receivedMessage = JsonConvert.DeserializeObject<TMessage>(receivedText);
                    await handler(receivedMessage);
                    channel.BasicAck(ea.DeliveryTag, false);
                };
                string consumerTag = channel.BasicConsume(config.QueueName, false, consumer); 
            });
        }
        
        public Task Stop() {
            return Task.Run(() => {
                channel.Close();
                connection.Close();
            });
        }
    }
}