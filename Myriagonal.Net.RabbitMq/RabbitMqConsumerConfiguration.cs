﻿namespace Myriagonal.Net.RabbitMq {
    public class RabbitMqConsumerConfiguration {
        public string ConnectionUrl { get; set; }
        public string QueueName { get; set; }
    }
}