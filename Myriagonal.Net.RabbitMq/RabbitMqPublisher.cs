﻿using System;
using System.Text;
using System.Threading.Tasks;
using Myriagonal.Net.Core.Messaging;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace Myriagonal.Net.RabbitMq {
    public class RabbitMqPublisher : Publisher
    {
        private readonly RabbitMqPublisherConfiguration _config;
        private readonly ConnectionFactory _connectionFactory;
        private IConnection _connection;
        private IModel _channel;

        public RabbitMqPublisher(RabbitMqPublisherConfiguration config)
        {
            _config = config;
            _connectionFactory = new ConnectionFactory
            {
                Uri = new Uri(_config.ConnectionUrl),
                AutomaticRecoveryEnabled = true,
                TopologyRecoveryEnabled = true
            };
        }

        public Task PublishAsync<T>(T message)
        {
            return Task.Run(() => {
                string json = JsonConvert.SerializeObject(message);
                byte[] data = Encoding.UTF8.GetBytes(json);
                _channel.TxSelect();
                _channel.BasicPublish(_config.ExchangeName, "", null, data);
                _channel.TxCommit();
            });
        }

        public Task Start() {
            return Task.Run(() => SyncStart());
        }

        private void SyncStart() {
            if (_connection == null || !_connection.IsOpen)
            {
                _connection = _connectionFactory.CreateConnection();
            }

            if (_channel == null || _channel.IsClosed)
            {
                _channel = _connection.CreateModel();
            }

            _channel.ExchangeDeclare(_config.ExchangeName, ExchangeType.Fanout, true);
            
            if (!string.IsNullOrEmpty(_config.QueueName))
            {
                _channel.QueueDeclare(_config.QueueName, true, false, false, null);
                _channel.QueueBind(_config.QueueName, _config.ExchangeName, "");
            }

        }
        
        public Task Stop() {
            return Task.Run(() => SyncStop());
        }

        private void SyncStop()
        {
            try
            {
                _channel.Close();
                _connection.Close();
            }
            finally
            {
                _channel = null;
                _connection = null;
            }
        }

        public void Dispose()
        {
            Stop();
        }
    }
}