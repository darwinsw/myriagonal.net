﻿namespace Myriagonal.Net.RabbitMq {
    public class RabbitMqPublisherConfiguration {
        public string ConnectionUrl { get; set; }
        public string ExchangeName { get; set; }
        public string QueueName { get; set; }
    }
}