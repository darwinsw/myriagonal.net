﻿namespace Myriagonal.Net.Core.Logging {
    public enum LogLevel {
        Trace, Debug, Info, Warn, Error
    }
}