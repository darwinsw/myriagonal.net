namespace Myriagonal.Net.Core.Logging {
    public interface LoggerFactory {
        Logger GetLogger<T>();
    }
}
