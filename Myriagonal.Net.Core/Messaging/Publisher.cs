﻿using System.Threading.Tasks;

namespace Myriagonal.Net.Core.Messaging {
    public interface Publisher {
        Task PublishAsync<T>(T message);

        Task Start();

        Task Stop();
    }
}